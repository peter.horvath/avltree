package avltree

/*
import (
  "fmt"
)
*/

func (n *AvlNode) fixHs() bool {
  h := n.height
  s := n.size

  lh := n.leftHeight()
  rh := n.rightHeight()

  if lh < rh {
    n.height = rh + 1
  } else {
    n.height = lh + 1
  }

  n.size = n.leftSize() + n.rightSize() + 1

  return n.height == h && n.size == s
}

func (n *AvlNode) fixHsRec() {
  var c *AvlNode
  for c = n; c != nil; c = c.parent {
    if c.fixHs() {
      break
    }
  }
}

func (n *AvlNode) rotateLeft() {
  if n.right == nil {
    panic("")
  }

  //fmt.Printf("l.h: %d h: %d r.l.h: %d r.h: %d r.r.h: %d\n", n.leftHeight(), n.height, n.right.leftHeight(), n.right.height, n.right.rightHeight())

  p := n.parent
  b := n.right
  c := b.left

  if p != nil {
    if p.left == n {
      p.left = b
    } else {
      p.right = b
    }
  }

  b.parent = p
  b.left = n

  n.parent = b
  n.right = c

  if c != nil {
    c.parent = n
  }

  n.fixHs()
  b.fixHsRec()
}

func (n *AvlNode) rotateRight() {
  if n.left == nil {
    panic("")
  }

  //fmt.Printf("l.l.h: %d l.h: %d l.r.h: %d h: %d r.h: %d\n", n.left.leftHeight(), n.left.height, n.left.rightHeight(), n.height, n.rightHeight())

  p := n.parent
  a := n.left
  c := a.right

  if p != nil {
    if p.left == n {
      p.left = a
    } else {
      p.right = a
    }
  }

  a.parent = p
  a.right = n

  n.parent = a
  n.left = c

  if c != nil {
    c.parent = n
  }

  n.fixHs()
  a.fixHsRec()
}

func (n *AvlNode) balance() bool {
  lh := n.leftHeight()
  rh := n.rightHeight()

  //fmt.Printf("node: %d, lh: %d, rh: %d, ", n.value.(int), lh, rh)
  if rh > lh + 1 {
    //fmt.Println("rotateLeft")
    if n.right.leftHeight() > n.right.rightHeight() {
      n.right.rotateRight()
    }
    n.rotateLeft()
    return true
  } else if lh > rh + 1 {
    //fmt.Println("rotateRight")
    if n.left.rightHeight() > n.left.leftHeight() {
      n.left.rotateLeft()
    }
    n.rotateRight()
    return true
  } else {
    //fmt.Println("no rotate")
    return false
  }
}

func (n *AvlNode) balanceRec() {
  c := n
  for {
    if c == nil {
      break
    }
    c.balance()
    c = c.parent
  }
}

func (t *AvlTree) fixRoot(n *AvlNode) {
  for {
    if n.parent == nil {
      break
    }
    n = n.parent
  }
  t.root = n
}

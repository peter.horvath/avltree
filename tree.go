// AvlTree methods out of the core logic

package avltree

// Clear removes all elements from the tree, keeping the
// current options and compare function
func (t *AvlTree) Clear() {
  t.Init(t.compare, t.flags)
}

// Size returns the number of elements in the tree
func (t *AvlTree) GetSize() int64 {
	if t.root == nil {
		return 0
	}
	return t.root.size
}

func (t *AvlTree) GetComparator() CompareFunc {
  return t.compare
}

func (t *AvlTree) SetComparator(c CompareFunc) {
  if t.GetSize() > 1 {
    panic("comparators can be changed only for tree sizes <= 1")
  }
  t.compare = c
}

func (t *AvlTree) HasByCompareFunc(v interface{}, c CompareFunc) bool {
  if t.root == nil {
    return false
  }
  return t.root.findChild(v, c) != nil
}

func (t *AvlTree) Has(v interface{}) bool {
  return t.HasByCompareFunc(v, t.compare)
}

func (t *AvlTree) FindByCompareFunc(p interface{}, c CompareFunc) *AvlNode {
  if t.root == nil {
    return nil
  } else {
    return t.root.findChild(p, c)
  }
}

func (t *AvlTree) Find(p interface{}) *AvlNode {
  return t.FindByCompareFunc(p, t.compare)
}

func (t *AvlTree) GetFirst() *AvlNode {
  if t.root == nil {
    return nil
  } else if t.root.left == nil {
    return t.root
  } else {
    return t.root.smallestChild()
  }
}

func (t *AvlTree) GetLast() *AvlNode {
  if t.root == nil {
    return nil
  } else if t.root.right == nil {
    return t.root
  } else {
    return t.root.highestChild()
  }
}

func (t *AvlTree) IsEmpty() bool {
  return t.root == nil
}

func (t *AvlTree) Remove(v interface{}) bool {
  if t.root == nil {
    return false
  } else {
    n := t.root.findChild(v, t.compare)
    if n == nil {
      return false
    } else {
      t.detachNode(n)
      return true
    }
  }
}

func (t *AvlTree) RemoveNode(n *AvlNode) {
  t.detachNode(n)
}

func (t *AvlTree) NodeAt(n int64) *AvlNode {
  if t.root == nil {
    return nil
  }
  return t.root.getChildByIdx(n)
}

func (t *AvlTree) At(n int64) interface{} {
  node := t.NodeAt(n)
  if node == nil {
    return nil
  } else {
    return node.GetValue()
  }
}

package avltree

func (t *AvlTree) UnsortedAddFirst(v interface{}) *AvlNode {
  newN := newAvlNode(v)
  if t.root == nil {
    t.root = newN
  } else if t.root.left == nil {
    t.addLeafLeft(t.root, newN)
  } else {
    t.addLeafLeft(t.root.smallestChild(), newN)
  }
  return newN
}

func (t *AvlTree) UnsortedAddLast(v interface{}) *AvlNode {
  newN := newAvlNode(v)
  if t.root == nil {
    t.root = newN
  } else if t.root.right == nil {
    t.addLeafRight(t.root, newN)
  } else {
    t.addLeafRight(t.root.highestChild(), newN)
  }
  return newN
}

func (t *AvlTree) UnsortedAddAfter(n *AvlNode, v interface{}) *AvlNode {
  newN := newAvlNode(v)
  if n.right == nil {
    t.addLeafRight(n, newN)
  } else if n.right.left == nil {
    t.addLeafLeft(n.right, newN)
  } else {
    t.addLeafLeft(n.right.smallestChild(), newN)
  }
  return newN
}

func (t *AvlTree) UnsortedAddBefore(n *AvlNode, v interface{}) *AvlNode {
  newN := newAvlNode(v)
  if n.left == nil {
    t.addLeafLeft(n, newN)
  } else if n.left.right == nil {
    t.addLeafRight(n.left, newN)
  } else {
    t.addLeafRight(n.left.smallestChild(), newN)
  }
  return newN
}

func (t *AvlTree) UnsortedInsert(idx int64, v interface{}) *AvlNode {
  if idx < 0 {
    panic ("negative idx is illegal")
  } else if idx == 0 {
    return t.UnsortedAddFirst(v)
  } else if idx <= t.GetSize() {
    return t.UnsortedAddAfter(t.NodeAt(idx - 1), v)
  } else {
    panic ("idx > size is illegal")
  }
}

func (n *AvlNode) UnsortedSetValue(v interface{}) *AvlNode {
  n.value = v
  return n
}

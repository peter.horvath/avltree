package avltree

// AvlNode is a node in the tree
type AvlNode struct {
	// left and right nodes
	left, right, parent *AvlNode

	// The height of this node
  height byte

	// The number of nodes in the subtree
	size int64

	// The contents of this node
	value interface{}
}

// Init initializes a node with the given value
func (n *AvlNode) init(value interface{}) *AvlNode {
	n.left = nil
	n.right = nil
  n.parent = nil
	n.height = 1
	n.size = 1
	n.value = value
	return n
}

// newNode returns an initialized AvlNode
func newAvlNode(value interface{}) *AvlNode {
  return new(AvlNode).init(value)
}

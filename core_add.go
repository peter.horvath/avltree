package avltree

func (t *AvlTree) Add(v interface{}) *AvlNode {
  if t.root == nil {
    n := newAvlNode(v)
    t.root = n
    return n
  }

  var toLeft bool
  n := t.root
  for {
    cr := t.compare(v, n.value)
    if cr < 0 {
      toLeft = true
    } else if cr > 0 {
      toLeft = false
    } else {
      if t.flags & AllowDuplicates == 0 {
        panic("no dupes")
        /*
        ret := n.value
        n.value = v
        return ret
        */
      } else {
        toLeft = n.leftSize() < n.rightSize()
      }
    }

    if toLeft {
      if n.left == nil {
        t.addLeafLeft(n, newAvlNode(v))
        return nil
      } else {
        n = n.left
      }
    } else {
      if n.right == nil {
        t.addLeafRight(n, newAvlNode(v))
        return nil
      } else {
        n = n.right
      }
    }
  }
}

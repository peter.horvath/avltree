package avltree

func (t *AvlTree) detachNode(n *AvlNode) {
  if n.left == nil && n.right == nil {
    if n.parent != nil {
      if n.isLeftChild() {
        n.parent.left = nil
      } else {
        n.parent.right = nil
      }
      n.parent.fixHsRec()
      n.parent.balanceRec()
      t.fixRoot(n.parent)
    } else {
      t.root = nil
    }
  } else if n.left == nil && n.right != nil {
    if n.parent == nil {
      t.root = n.right
      n.right.parent = nil
    } else {
      if n.isLeftChild() {
        n.parent.left = n.right
      } else {
        n.parent.right = n.right
      }
      n.right.parent = n.parent
      n.parent.fixHsRec()
      n.parent.balanceRec()
      t.fixRoot(n.parent)
    }
  } else if n.left != nil && n.right == nil {
    if n.parent == nil {
      t.root = n.left
      n.left.parent = nil
    } else {
      if n.isLeftChild() {
        n.parent.left = n.left
      } else {
        n.parent.right = n.left
      }
      n.left.parent = n.parent
      n.parent.fixHsRec()
      n.parent.balanceRec()
      t.fixRoot(n.parent)
    }
  } else {
    ls := n.left.size
    rs := n.right.size
    var nxt *AvlNode
    var nextRootFrom *AvlNode
    if ls < rs {
      nextRootFrom = n.left
      nxt = n.right
      if nxt.left != nil {
        nxt = nxt.smallestChild()
      }
    } else {
      nextRootFrom = n.right
      nxt = n.left
      if nxt.right != nil {
        nxt = nxt.highestChild()
      }
    }
    t.detachNode(nxt)
    n.substituteWith(nxt)
    t.fixRoot(nextRootFrom)
  }

  n.parent = nil
  n.right = nil
  n.left = nil
  n.size = 1
  n.height = 1
}

func (n *AvlNode) substituteWith(nxt *AvlNode) {
  if n.parent != nil {
    if n.isLeftChild() {
      n.parent.left = nxt
    } else {
      n.parent.right = nxt
    }
  }
  if n.left != nil {
    n.left.parent = nxt
  }
  if n.right != nil {
    n.right.parent = nxt
  }

  nxt.parent = n.parent
  nxt.left = n.left
  nxt.right = n.right
  nxt.size = n.size
  nxt.height = n.height

  n.parent = nil
  n.left = nil
  n.right = nil
  n.size = 1
  n.height = 1
}

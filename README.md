If you are looking for a clean, simple, effective avltree implementation, you found it.

core_*.go does everything (add-remove-balance), all other is only syntax sugar.

The whole thing is in O(log(n)). The stresstest is slow because it does O(n^2) tree operations
as the tree is growing, so its speed slows as O(n^3*log(n)) with the iterations.

But the induvidual operations are still O(log(n)).

---

Usage is very simple:

1. Create a new tree, with your comparison function:

t := NewTree(myCompare)

Where myCompare has the type AvlTree.Compare : func (a interface{}, b interface{}) int .

It should result a negative int a < b, a positive if a > b and 0 if a == b.

2. Add a new element into the tree:

t.Add(5)

Will add "5" into the tree. Note,

- *it will call your comparator with "5", possibly multiple times!*
- as in Go, all parameter transfer happens by-value, it can also *copy* "5" with multiple times.
. *it may be useful, and perfect well working, if you use create your tree actually from pointers.*

3. Remove an element from the tree:

t.Remove(5)

The same as for "Add".

4. Find, Has and similar methods are detailed later. If you have a question for any of them, I will write
this part of the doc first. :-)

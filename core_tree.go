/*
An AVL tree (Adel'son-Vel'skii & Landis) is a binary search
tree in which the heights of the left and right subtrees
of the root differ by at most one and in which the left
and right subtrees are again AVL trees.

With each node of an AVL tree is associated a balance factor
that is Left High, Equal, or Right High according,
respectively, as the left subtree has height greater than,
equal to, or less than that of the right subtree.

The AVL tree is, in practice, balanced quite well.  It can
(at the worst case) become skewed to the left or right,
but never so much that it becomes inefficient.  The
balancing is done as items are added or deleted.

It is safe to iterate or search a tree from multiple threads
provided that no threads are modifying the tree.

See also:	Robert L. Kruse, Data Structures and Program Design, 2nd Ed., Prentice-Hall
*/
package avltree

// tree options
const (
	AllowDuplicates = 1
)

type CompareFunc func(a interface{}, b interface{}) int

// AvlTree object
type AvlTree struct {

	// root of the tree
	root *AvlNode

  // comparator function
  compare CompareFunc

	// options controlling behavior
	flags byte
}

// Initialize or reset a AvlTree
func (t *AvlTree) Init(c CompareFunc, flags byte) *AvlTree {
  t.compare = c
	t.root = nil
	t.flags = flags
	return t
}

// Return an initialized tree
func NewAvlTree(c CompareFunc, flags byte) *AvlTree {
  return new(AvlTree).Init(c, flags)
}

// Utility methods used by the core logic

package avltree

func (n *AvlNode) isLeftChild() bool {
  return (n.parent != nil) && (n == n.parent.left)
}

func (n *AvlNode) isRightChild() bool {
  return (n.parent != nil) && (n == n.parent.right)
}

func (n *AvlNode) leftSize() int64 {
  if n.left == nil {
    return 0
  } else {
    return n.left.size
  }
}

func (n *AvlNode) rightSize() int64 {
  if n.right == nil {
    return 0
  } else {
    return n.right.size
  }
}

func (n *AvlNode) leftHeight() byte {
  if n.left == nil {
    return 0
  } else {
    return n.left.height
  }
}

func (n *AvlNode) rightHeight() byte {
  if n.right == nil {
    return 0
  } else {
    return n.right.height
  }
}

// gives back the smallest node _below_ the node. If there is none,
// returns nil
func (n *AvlNode) smallestChild() *AvlNode {
  if n.left == nil {
    return nil
  }
  for {
    n = n.left
    if n.left == nil {
      return n
    }
  }
}

// gives back the highest node _below_ the node. If there is none,
// returns nil
func (n *AvlNode) highestChild() *AvlNode {
  if n.right == nil {
    return nil
  }
  for {
    n = n.right
    if n.right == nil {
      return n
    }
  }
}

// can be called **only** if to.left == nil !
func (t *AvlTree) addLeafLeft(to *AvlNode, what *AvlNode) {
  to.left = what
  what.parent = to
  to.fixHsRec()
  to.balanceRec()
  t.fixRoot(to)
}

// can be called **only** if to.right == nil !
func (t *AvlTree) addLeafRight(to *AvlNode, what *AvlNode) {
  to.right = what
  what.parent = to
  to.fixHsRec()
  to.balanceRec()
  t.fixRoot(to)
}

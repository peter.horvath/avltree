// AvlNode methods out of the core logic

package avltree

func (n *AvlNode) findChild(v interface{}, c CompareFunc) *AvlNode {
  for {
    if n == nil {
      return nil
    }
    r := c(v, n.value)
    if r < 0 {
      n = n.left
    } else if r > 0 {
      n = n.right
    } else {
      return n
    }
  }
}

func (n *AvlNode) GetNext() *AvlNode {
  if n.right != nil {
    r := n.right.smallestChild()
    if r == nil {
      return n.right
    } else {
      return r
    }
  } else if n.parent == nil {
    return nil
  } else {
    r := n
    for {
      if r.isLeftChild() {
        return r.parent
      }
      r = r.parent
      if r == nil {
        return nil
      }
    }
  }
}

func (n *AvlNode) GetPrev() *AvlNode {
  if n.left != nil {
    r := n.left.highestChild()
    if r == nil {
      return n.left
    } else {
      return r
    }
  } else if n.parent == nil {
    return nil
  } else {
    r := n
    for {
      if r.isLeftChild() {
        return r.parent
      }
      r = r.parent
      if r == nil {
        return nil
      }
    }
  }
}

func (n *AvlNode) GetValue() interface{} {
  return n.value
}

func (n *AvlNode) GetIdx() int64 {
  r := n.leftSize() + 1
  for {
    if n.isRightChild() {
      r += n.parent.leftSize() + 1
    }
    n = n.parent
    if n == nil {
      break
    }
  }
  return r
}

func (node *AvlNode) getChildByIdx(n int64) *AvlNode {
  if (n < 0) || (n >= node.size) {
    return nil
  } else if n < node.leftSize() {
    return node.left.getChildByIdx(n)
  } else if n == node.leftSize() {
    return node
  } else {
    return node.right.getChildByIdx(n - node.leftSize() - 1)
  }
}
